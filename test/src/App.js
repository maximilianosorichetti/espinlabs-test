import { useState } from 'react'
import './App.css';

function App() {
  const [ data, setData ] = useState(["01 - Buenos Aires", "02 - Córdoba", "03 - Misiones"])
  const [ inputValue, setInputValue ] = useState("")

  const deleteZone = (zone) => {
    const filteredZone = data.filter(dataZone => dataZone !== zone)
    setData(filteredZone)
  }

  function reverseString(str) {
    return str.split("").reverse().join("");
  }

  function esCapicua(inputValue) {
    if (reverseString(inputValue) === inputValue) {
      alert("el string es capicupa!")
      return(true);
    }
    else {  
      alert("el string no es capicupa!")
      return(false);
    } 
}

  const handleChangeInput = (event) => {
    setInputValue(event.target.value)
  }
    
  return (
    <div className="container">
      <div>
        <table className="table">
          {data.map((zone, index) => {
            return (
            <tr key={index}>
              <td>{zone}</td>
              <td className="col2">
                <button onClick={() => deleteZone(zone)} type="button">
                  Borrar
                </button>
                <button>Editar</button>
              </td>
            </tr>
          )})}
        </table>
      </div>
      <div className="capicua">
        <input className="input" onChange={handleChangeInput}></input>
        <button onClick={() => esCapicua(inputValue)}>Ver si es Capicua</button>
      </div>
        </div>
  );
}

export default App;
